extends Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	var animations: PackedStringArray = $AnimationPlayerEnergy.get_animation_list()
	$AnimationPlayerEnergy.play(animations[randi_range(1, animations.size() - 1)])
	$AnimationPlayerEnergy.seek(randf_range(0, $AnimationPlayerEnergy.current_animation_length))
	
	$AnimationPlayerAttenuation.seek(randf_range(0, $AnimationPlayerAttenuation.current_animation_length))
	
	$SpotLight3D.spot_angle = randi_range(70, 80)



func _on_animation_player_energy_animation_finished(anim_name):
	await get_tree().create_timer(randi_range(0,2)).timeout
	$AnimationPlayerEnergy.play(anim_name)
